package com.kyle.bbs.dao;

import com.kyle.bbs.entity.Follow;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface FollowDao extends Mapper<Follow> {

    Integer insertAndUpdateFollowStatus(@Param("userId") String userId, @Param("targetId") String targetId);

    Integer updateFollowStatus(@Param("userId") String userId, @Param("targetId") String targetId, @Param("status") Integer status);

    Long selectUserFollowCount(@Param("targetId") String targetId, @Param("status") Integer status);

    Follow selectFollowByUserAndTarget(@Param("userId") String userId, @Param("targetId") String targetId);

    List<String> selectUserFollowedIds(@Param("userId") String userId, @Param("status") Integer status);
}
