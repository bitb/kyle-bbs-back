package com.kyle.bbs.dao;

import com.kyle.bbs.entity.DiscussPost;
import com.kyle.bbs.entity.transientEntities.Params;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface DiscussPostDao extends Mapper<DiscussPost> {

    List<DiscussPost> findBySearch(@Param("params") Params params);

    List<DiscussPost> findPostsByTagId(@Param("params") Params params);

    Integer updateDiscussPostStatusById(@Param("id") Integer id, @Param("status") Integer status);

    List<DiscussPost> findTopPostList();

    List<DiscussPost> findPostByUserId(@Param("params") Params params);

    List<DiscussPost> selectByIds(@Param("postIds") List<String> postIds);

    Integer findPostCountByUserId(@Param("userId") Integer id, @Param("params") Params params);

    Integer updateScore(@Param("postId") int postId, @Param("score") double score);
}
