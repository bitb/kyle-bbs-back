package com.kyle.bbs.dao;

import com.kyle.bbs.entity.Tag;
import com.kyle.bbs.entity.transientEntities.Params;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface TagDao extends Mapper<Tag> {

    List<Tag> findTagsByPostId(@Param("postId") Integer postId, @Param("status") Integer status);

    List<Tag> findAllTags(@Param("params") Params params);

    Integer setTagsDeletedByPostId(@Param("postId") Integer postId);

    Integer insertAndUpdatePostTags(@Param("tags") List<Tag> tags, @Param("postId") Integer postId);

    Integer insertPostTags(@Param("tags") List<Tag> tags, @Param("postId") Integer postId);

}
