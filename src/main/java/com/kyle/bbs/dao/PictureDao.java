package com.kyle.bbs.dao;

import com.kyle.bbs.entity.Picture;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface PictureDao extends Mapper<Picture> {

    List<Picture> findPictureListByEntityId(@Param("entityType") Integer entityType, @Param("entityId") String entityId);

    Integer setDeletedPictureListByEntityId(@Param("entityType") Integer entityType, @Param("entityId") String entityId);

    Integer insertAndUpdatePictureListByPictureList(@Param("pictures") List<Picture> pictures);
}
