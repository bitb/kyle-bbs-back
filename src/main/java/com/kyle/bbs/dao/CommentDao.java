package com.kyle.bbs.dao;

import com.kyle.bbs.entity.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface CommentDao extends Mapper<Comment> {

    List<Comment> findByEntity(@Param("entityType") Integer entityType, @Param("entityId") String entityId);

    Integer selectCountByEntity( @Param("postId") String postId);

    Integer updateStatus(@Param("entityId") String entityId, @Param("status") Integer status);

    List<String> selectCommentedPostIdsByUser(@Param("userId") String userId, @Param("status") Integer status);
}
