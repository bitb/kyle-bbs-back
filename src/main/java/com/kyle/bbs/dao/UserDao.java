package com.kyle.bbs.dao;

import com.kyle.bbs.entity.Password;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.entity.transientEntities.Params;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface UserDao extends Mapper<User> {

    List<User> findBySearch(@Param("params") Params params);

    @Update("update user set avatar = #{avatar} where name = #{name}")
    Integer updateAvatarByName(@Param("avatar") String avatar, @Param("name") String name);

    @Select("select * from user where name = #{name} limit 1")
    User findByName(@Param("name") String name);

    @Select("select * from user where nick_name = #{nickName} limit 1")
    User findByNickName(@Param("name") String nickName);

    @Select("select * from user where name = #{name} and password = #{password} limit 1")
    User findByNameAndPassword(@Param("name")String name, @Param("password") String password);

    @Select("select name from user where id = #{id}")
    String findNameById(@Param("id") String id);

    Integer updateUserPwd(@Param("password")Password password);

    List<User> findByIds(@Param("params") Params params, @Param("ids") List<String> ids);
}
