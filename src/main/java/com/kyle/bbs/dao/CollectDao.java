package com.kyle.bbs.dao;

import com.kyle.bbs.entity.Collect;
import com.kyle.bbs.entity.Follow;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface CollectDao extends Mapper<Collect> {

    Integer insertAndUpdateCollectStatus(@Param("userId") String userId,
                                        @Param("entityType") Integer entityType, @Param("entityId") String  entityId);

    Integer updateCollectStatus(@Param("userId") String userId,
                                @Param("entityType") Integer entityType, @Param("entityId") String  entityId,
                                @Param("status") Integer status);

    Long selectEntityCollectCount(@Param("entityType") Integer entityType, @Param("entityId") String  entityId,
                                  @Param("status") Integer status);

    Collect selectCollectByUserAndTarget(@Param("userId") String userId,
                                        @Param("entityType") Integer entityType, @Param("entityId") String  entityId);

    List<String> selectUserCollectedEntityIds(@Param("userId") String userId,
                                              @Param("entityType") Integer entityType, @Param("status") Integer status);
}
