package com.kyle.bbs.common.factory;

import com.kyle.bbs.common.BBSConstants;
import org.springframework.stereotype.Component;

@Component
public class EntityTypeFactory implements BBSConstants {

    public static Integer entityTypeStrToInt(String entityType) {
        switch (entityType){
            case "discusspost":
                return ENTITY_TYPE_POST;
            case "comment":
                return ENTITY_TYPE_COMMENT;
            case "user":
                return ENTITY_TYPE_USER;
            case "message":
                return ENTITY_TYPE_MESSAGE;
            default:
                return -1;
        }
    }

    public static String entityTypeIntToStr(Integer entityType) {
        switch (entityType){
            case 0:
                return "discusspost";
            case 1:
                return "comment";
            case 2:
                return "user";
            case 3:
                return "message";
            default:
                return null;
        }
    }
}
