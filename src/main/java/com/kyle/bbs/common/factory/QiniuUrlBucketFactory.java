package com.kyle.bbs.common.factory;

import com.kyle.bbs.common.BBSConstants;
import org.springframework.stereotype.Component;

@Component
public class QiniuUrlBucketFactory implements BBSConstants {
    public String[] getUrlBucket(String entityType) {
        if (entityType.equals("discusspost")) {
            return new String[] {QINIU_DISCUSSPOST_URL, DISCUSSPOST_BUCKET_NAME};
        } else if (entityType.equals("avatar")) {
            return new String[] {QINIU_AVATAR_URL, AVATAR_BUCKET_NAME};
        } else if (entityType.equals("share")) {
            return new String[] {QINIU_SHARE_URL, SHARE_BUCKET_NAME};
        }
        return null;
    }
}
