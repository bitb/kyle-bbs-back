package com.kyle.bbs.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface BBSConstants {

    /**
     * 关注状态：
     * FOLLOWED：0
     * UNFOLLOWED：1
     */
    Integer FOLLOW_STATUS_FOLLOWED = 0;
    Integer FOLLOW_STATUS_UNFOLLOWED = 1;

    /**
     * 收藏状态：
     * COLLECTED：0
     * UNCOLLECTED：1
     */
    Integer COLLECTION_STATUS_COLLECTED = 0;
    Integer COLLECTION_STATUS_UNCOLLECTED = 1;

    /**
     * 图片所属实体类型:
     * POST:帖子
     * COMMENT:评论
     * USER:用户
     * MESSAGE:消息和通知
     */
    Integer ENTITY_TYPE_POST = 0;
    Integer ENTITY_TYPE_COMMENT = 1;
    Integer ENTITY_TYPE_USER = 2;
    Integer ENTITY_TYPE_MESSAGE = 3;

    /**
     * 帖子状态：
     * DELETED：被删除
     * OK：正常
     * TOPPED：被置顶
     * BLOCKED：被屏蔽
     */
    Integer POST_STATUS_DELETED = -1;
    Integer POST_STATUS_OK = 0;
    Integer POST_STATUS_TOPPED = 1;
    Integer POST_STATUS_BLOCKED = 2;
    List<Integer> POSTS_AVAILABLE_FOR_NOT_ADMINS = new ArrayList<Integer>(){{
        add(POST_STATUS_OK);
        add(POST_STATUS_TOPPED);
    }};

    /**
     * 评论类型：
     * COMMENT：评论
     * REPLY：回复
     * MESSAGE：留言
     */
    Integer COMMENT_TYPE_COMMENT = 0;
    Integer COMMENT_TYPE_REPLY = 1;
    Integer COMMENT_TYPE_MESSAGE = 2;

    /**
     * 评论状态：
     * DELETED：删除
     * OK：正常
     * SHADOWED：隐藏
     */
    Integer COMMENT_STATUS_DELETED = -1;
    Integer COMMENT_STATUS_OK = 0;
    Integer COMMENT_STATUS_SHADOWED = 1;

    /**
     * 标签状态：
     * DELETED：删除
     * OK：正常
     * SHADOWED：隐藏
     * DEPRECATED：弃用
     */
    Integer TAG_STATUS_OK = 0;
    Integer TAG_STATUS_SHADOWED = 1;
    Integer TAG_STATUS_DELETED = 2;
    Integer TAG_STATUS_DEPRECATED = 3;

    /**
     * 帖子标签状态：
     * OK：正常
     * DELETED：删除
     */
    Integer POST_TAG_STATUS_OK = 0;
    Integer POST_TAG_STATUS_DELETED = 1;

    /**
     * 用户类型：
     * COMMON：普通
     * ADMIN：管理员
     * MODERATOR：版主
     */
    Integer USER_TYPE_COMMON = 0;
    Integer USER_TYPE_ADMIN = 1;
    Integer USER_TYPE_MODERATOR = 2;

    /**
     * 七牛云
     */
    // 头像外链域名
    String QINIU_AVATAR_URL = "s9x45jz27.hn-bkt.clouddn.com";
    // 头像存储空间名称
    String AVATAR_BUCKET_NAME = "kylebbs--avatar--0";
    // 分享外链域名
    String QINIU_SHARE_URL = "s9x4vjn79.hn-bkt.clouddn.com";
    // 分享存储空间名称
    String SHARE_BUCKET_NAME = "kylebbs--share--0";
    // 分享外链域名
    String QINIU_DISCUSSPOST_URL = "sc4xrxzrf.hn-bkt.clouddn.com";
    // 帖子存储空间名称
    String DISCUSSPOST_BUCKET_NAME = "kylebbs--discusspost--1";
    // AccessKey
    String ACCESS_KEY = "Vt_dQWRinKnsxbXOlvumwgnFkuS6wI4FVQZMxbr5";
    // SecretKey
    String SECRET_KEY = "OarqeIeg1wxoc4KU3k96jzoFJM2qdieHSu_cFL8v";

    /**
     * 请求结果类型
     */
    String SUCCESS = "0";
    String ERROR = "-1";
    String NEED_LOGIN_ADMIN = "-2";
    String NEED_LOGIN_COMMON = "-3";

    String REDIRECT_TO_MAIN = "-4";
}
