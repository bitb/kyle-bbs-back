package com.kyle.bbs.exception;

import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.Result;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler implements BBSConstants {
    public static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    // 统一异常处理@ExceptionHandler，主要用于Exception
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(HttpServletRequest request, Exception e){
        logger.error("异常信息: ", e);
        return Result.error("系统异常");
    }

    @ExceptionHandler(CustomException.class)
    @ResponseBody
    public Result customError(HttpServletRequest request, CustomException e) {
        if (e.getErrCode() != null) {
            logger.error("异常! code:{" + e.getErrCode() + "} ，异常信息: " + e.getMsg());
            return Result.error(e.getMsg(), e.getErrCode());
        }
        logger.error("异常信息: " + e.getMsg());
        return Result.error(e.getMsg());
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public Result unAuthException(HttpServletRequest request, UnauthorizedException e) {
        logger.error("异常信息: " + e.getMessage());
        return Result.error("您不具备执行相应操作的权限", REDIRECT_TO_MAIN);
    }

    @ExceptionHandler(AuthorizationException.class)
    @ResponseBody
    public Result unAuthenticatedException(HttpServletRequest request, AuthorizationException e) {
        logger.error("异常信息: " + e.getMessage());
        return Result.error("请先登录", NEED_LOGIN_COMMON);
    }
}
