package com.kyle.bbs.exception;

public class CustomException extends RuntimeException{
    private String msg;

    private String errCode;

    public CustomException(String msg) {
        this.msg = msg;
    }

    public CustomException(String msg, String errCode) {
        this.msg = msg;
        this.errCode = errCode;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
