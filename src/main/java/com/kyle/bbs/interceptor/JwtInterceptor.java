package com.kyle.bbs.interceptor;

import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.service.UserService;
import com.kyle.bbs.utils.JwtTokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * JWT拦截器
 */
@Component
public class JwtInterceptor implements HandlerInterceptor, BBSConstants {

    public static final Logger logger = LoggerFactory.getLogger(JwtInterceptor.class);

    @Resource
    private UserService userService;

    @Resource
    private HostHolder hostHolder;

    @Resource
    private JwtTokenUtils jwtTokenUtils;

    /*@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // 1.从HTTP请求的header中获取token
        String token = request.getHeader("token");
        if (StrUtil.isBlank(token)) {
            // 如果没拿到，再去参数里拿一波试试 /user/?token=xxxx
            token = request.getParameter("token");
        }

        // 2.开始执行认证
        if (StrUtil.isBlank(token)) {
            throw new CustomException("无token，请重新登录");
        }
        // 获取token中的userId
        String userId;
        User user;

        try {
            userId = JWT.decode(token).getAudience().get(0);
            // 根据token中的userId查询数据库
            user = userService.findById(Integer.parseInt(userId));
        } catch (Exception e) {
            String errMsg = "token验证失败，请重新登录";
            logger.error(errMsg + ", token=" + token, e);
            throw new CustomException(errMsg);
        }
        if (user == null) {
            throw new CustomException("用户不存在，请重新登录");
        }

        jwtTokenUtils.verifyToken(token, user);

        if (request.getRequestURI().contains("admin")
                && !request.getRequestURI().contains("/admin/login")
                && !request.getRequestURI().contains("/admin/register")) {
            if (!user.getType().equals(USER_TYPE_ADMIN)) {
                System.out.println("1");
                throw new CustomException("你没有相应的权限");
            }
        }

        // 认证成功，在本次请求中存入User对象
        user.setToken(token);
        hostHolder.setUser(user);
        return true;
    }*/

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        hostHolder.clear();
    }
}
