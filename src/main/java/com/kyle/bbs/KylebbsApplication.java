package com.kyle.bbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.kyle.bbs.dao")
@EnableScheduling
public class KylebbsApplication {

    public static void main(String[] args) {
        SpringApplication.run(KylebbsApplication.class, args);
    }

}
