package com.kyle.bbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.dao.DiscussPostDao;
import com.kyle.bbs.entity.DiscussPost;
import com.kyle.bbs.entity.Tag;
import com.kyle.bbs.entity.transientEntities.CollectParams;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.entity.Picture;
import com.sun.istack.internal.NotNull;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class DiscussPostService implements BBSConstants {

    @Resource
    private DiscussPostDao discussPostDao;

    @Resource
    private PictureService pictureService;

    @Resource
    private LikeService likeService;

    @Resource
    private TagService tagService;

    @Resource
    private UserService userService;

    @Resource
    private CollectService collectService;

    @Resource
    private HostHolder hostHolder;

    // 根据用户id查询帖子
    public PageInfo<DiscussPost> findPostByUserId(Params params) {
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页配置进行查询
        List<DiscussPost> list = discussPostDao.findPostByUserId(params);
        return PageInfo.of(processPostInfoForLists(list));
    }

    // 根据标题和内容分页查询帖子列表
    public PageInfo<DiscussPost> findBySearch(Params params) {
        // 开启分页查询
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页配置进行查询
        List<DiscussPost> list = discussPostDao.findBySearch(params);
        return PageInfo.of(processPostInfoForLists(list));
    }

    // 根据标签分页查询帖子列表
    public PageInfo<DiscussPost> findByTag(Params params) {
        // 开启分页查询
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页配置进行查询
        List<DiscussPost> list = discussPostDao.findPostsByTagId(params);
        return PageInfo.of(processPostInfoForLists(list));
    }

    // 查找top10热帖
    public List<DiscussPost> findTopPostList() {
        return discussPostDao.findTopPostList();
    }

    // 根据ID查询帖子内容
    public DiscussPost findById(Integer id) {
        DiscussPost discussPost = discussPostDao.selectByPrimaryKey(id);
        if (discussPost == null) {
            return null;
        }
        discussPost.setLikeCount(likeService.findEntityLikeCount(ENTITY_TYPE_POST, id));
        discussPost.setLikeStatus(likeService.findEntityLikeStatus(hostHolder.getUser().getId(), ENTITY_TYPE_POST, id));
        List<Picture> pictures = pictureService.findPictureListByEntityId(ENTITY_TYPE_POST, String.valueOf(discussPost.getId()));
        discussPost.setPictures(pictures);
        if (Objects.equals(discussPost.getUserId(), "-1")) {
            discussPost.setNickName("管理员");
        } else {
            discussPost.setNickName(userService.findById(Integer.valueOf(discussPost.getUserId())).getNickName());
        }

        // 携带收藏情况
        CollectParams collectParams = new CollectParams();
        collectParams.setCollectStatus(collectService.findCollectStatus(ENTITY_TYPE_POST, String.valueOf(discussPost.getId())));
        collectParams.setCollectCount(collectService.findCollectCountByEntity(ENTITY_TYPE_POST, String.valueOf(discussPost.getId())));
        discussPost.setCollectParams(collectParams);

        return discussPost;
    }

    public DiscussPost findSimplyById(Integer id) {
        return discussPostDao.selectByPrimaryKey(id);
    }

    // 向数据库中插入帖子信息
    public void add(DiscussPost discussPost) {
        discussPost.setCreateTime(new Date());
        discussPostDao.insertSelective(discussPost);
    }

    // 更新帖子信息
    public void update(DiscussPost discussPost) {
        discussPost.setStatus(null);
        discussPost.setUserId(null);
        discussPost.setCreateTime(null);
        discussPostDao.updateByPrimaryKeySelective(discussPost);
    }

    public void adminUpdate(DiscussPost discussPost) {
        discussPost.setUserId(null);
        discussPost.setCreateTime(null);
        discussPostDao.updateByPrimaryKeySelective(discussPost);
    }

    // 更新帖子状态
    public Integer updateStatus(Integer id, Integer status) {
        return discussPostDao.updateDiscussPostStatusById(id, status);
    }

    // 根据ID从数据库中删除帖子
    public void delete(Integer id) {
        discussPostDao.deleteByPrimaryKey(id);
    }

    public PageInfo<DiscussPost> findPostByIds(Params params, List<String> postIds) {
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        if (postIds == null || postIds.isEmpty()) {
            return PageInfo.of(new ArrayList<>());
        }
        List<DiscussPost> list = discussPostDao.selectByIds(postIds);
        for (DiscussPost post : list) {
            CollectParams collectParams = new CollectParams();
            collectParams.setCollectStatus(collectService.findCollectStatus(ENTITY_TYPE_POST, String.valueOf(post.getId())));
            collectParams.setCollectCount(collectService.findCollectCountByEntity(ENTITY_TYPE_POST, String.valueOf(post.getId())));
            post.setCollectParams(collectParams);
        }
        return PageInfo.of(processPostInfoForLists(list));
    }

    @NotNull
    private List<DiscussPost> processPostInfoForLists(List<DiscussPost> list) {
        for (DiscussPost post : list) {
            post.setLikeCount(likeService.findEntityLikeCount(ENTITY_TYPE_POST, post.getId()));
            post.setPictures(pictureService.findPictureListByEntityId(ENTITY_TYPE_POST, String.valueOf(post.getId())));
            List<Tag> tags = tagService.findTagsByPostId(post.getId(), POST_TAG_STATUS_OK);
            post.setTags(tags);
            if (Objects.equals(post.getUserId(), "-1")) {
                post.setNickName("管理员");
            } else {
                post.setNickName(userService.findById(Integer.valueOf(post.getUserId())).getNickName());
            }

            // 携带收藏情况
            CollectParams collectParams = new CollectParams();
            collectParams.setCollectStatus(collectService.findCollectStatus(ENTITY_TYPE_POST, String.valueOf(post.getId())));
            collectParams.setCollectCount(collectService.findCollectCountByEntity(ENTITY_TYPE_POST, String.valueOf(post.getId())));
            post.setCollectParams(collectParams);
        }
        return list;
    }

    public Integer findUserPostCount(Integer id, Params params) {
        return discussPostDao.findPostCountByUserId(id, params);
    }

    public Integer updateScore(int postId, double score) {
        return discussPostDao.updateScore(postId, score);
    }
}
