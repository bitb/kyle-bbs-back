package com.kyle.bbs.service;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.dao.CommentDao;
import com.kyle.bbs.dao.UserDao;
import com.kyle.bbs.entity.Comment;
import com.kyle.bbs.entity.transientEntities.Params;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CommentService implements BBSConstants {

    @Resource
    private CommentDao commentDao;

    @Resource
    private UserDao userDao;

    @Resource
    private LikeService likeService;

    public PageInfo<Comment> findCommentByEntity(Integer entityType, String entityId, Params params) {
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        // 查询评论列表
        List<Comment> comments = commentDao.findByEntity(entityType, entityId);

        // 从Redis获取点赞数
        for (Comment comment : comments) {
            if (entityType.equals(COMMENT_TYPE_COMMENT)){
                comment.setLikeCount(likeService.findEntityLikeCount(COMMENT_TYPE_COMMENT, comment.getId()));
            } else {
                comment.setLikeCount(likeService.findEntityLikeCount(COMMENT_TYPE_REPLY, comment.getId()));
            }
        }

        for (Comment comment : comments) {

            if (COMMENT_TYPE_REPLY.equals(entityType)) {
                break;
            }

            // 查询每个评论的回复列表
            Params replyParams = comment.getParams();
            if (ObjectUtil.isEmpty(replyParams)) {
                replyParams = new Params();
                replyParams.setPageNum(1);
                replyParams.setPageSize(3);
            }

            PageHelper.startPage(replyParams.getPageNum(), replyParams.getPageSize());
            List<Comment> replies = commentDao.findByEntity(COMMENT_TYPE_REPLY, String.valueOf(comment.getId()));

            // 从Redis获取点赞数
            for (Comment reply : replies) {
                reply.setLikeCount(likeService.findEntityLikeCount(COMMENT_TYPE_REPLY, reply.getId()));
            }

            PageInfo<Comment> replyPageInfo = PageInfo.of(replies);
            comment.setReplies(replyPageInfo);
        }
        PageInfo<Comment> commentPageInfo = PageInfo.of(comments);
        return commentPageInfo;
    }

    public Integer addComment(Comment comment) {
        comment.setId(null);
        return commentDao.insertSelective(comment);
    }

    public Integer findCommentCount(String postId) {
        return commentDao.selectCountByEntity(postId);
    }

    public Integer updateStatus(String entityId, Integer status) {
        return commentDao.updateStatus(entityId, status);
    }

    public List<String> findUserCommentedPostIds(String userId, Integer status) {
        return commentDao.selectCommentedPostIdsByUser(userId, status);
    }
}
