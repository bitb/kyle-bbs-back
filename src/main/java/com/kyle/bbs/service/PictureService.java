package com.kyle.bbs.service;

import com.kyle.bbs.dao.PictureDao;
import com.kyle.bbs.entity.Picture;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class PictureService {

    @Resource
    private PictureDao pictureDao;

    public List<Picture> findPictureListByEntityId(Integer entityType, String entityId) {
        return pictureDao.findPictureListByEntityId(entityType, entityId);
    }

    public void add(List<Picture> pictures, Integer entityType, String entityId) {
        for (Picture picture : pictures) {
            picture.setEntityType(entityType);
            picture.setEntityId(entityId);
            pictureDao.insertSelective(picture);
        }
    }

    public void update(List<Picture> pictures, Integer entityType, String entityId) {
        pictureDao.setDeletedPictureListByEntityId(entityType, entityId);
        for (Picture picture : pictures) {
            picture.setEntityType(entityType);
            picture.setEntityId(entityId);
        }
        if (!pictures.isEmpty()){
            pictureDao.insertAndUpdatePictureListByPictureList(pictures);
        }
    }
}
