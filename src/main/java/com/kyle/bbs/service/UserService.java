package com.kyle.bbs.service;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.dao.DiscussPostDao;
import com.kyle.bbs.dao.UserDao;
import com.kyle.bbs.entity.DiscussPost;
import com.kyle.bbs.entity.Password;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.entity.transientEntities.FollowParams;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.exception.CustomException;
import com.kyle.bbs.utils.BBSUtil;
import com.kyle.bbs.utils.JwtTokenUtils;
import com.kyle.bbs.utils.RedisKeyUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class UserService implements BBSConstants {

    @Resource
    private UserDao userDao;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private HostHolder hostHolder;

    @Resource
    private FollowService followService;

    public List<User> findAll(){
        return userDao.selectAll();
    }

    public PageInfo<User> findBySearch(Params params) {
        // 开启分页查询
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        // 接下来的查询会自动按照当前开启的分页配置进行查询
        List<User> list = userDao.findBySearch(params);
        return PageInfo.of(list);
    }

    public void add(User user) {
        // 进行重复性判断，同一名字的用户不允许重复新增：在数据库通过用户名查询.
        User foundUser = userDao.findByName(user.getName());
        if (foundUser != null) {
            // 说明已经有了，需要提示前端不允许重复新增
            throw new CustomException("该用户名已存在！");
        }
        if (user.getName() == null || "".equals(user.getName())) {
            throw new CustomException("用户名不能为空！");
        }
        // 初始化一个密码
        if (user.getPassword() == null) {
            user.setPassword("123456");
        }
        // 初始化一个昵称
        if (user.getNickName() == null) {
            user.setNickName(user.getName());
        }
        // 获得md5密码摘要
        user.setSalt(BBSUtil.generateUUID().substring(0,5));
        String pwd = BBSUtil.md5(user.getPassword() + user.getSalt());
        user.setPassword(pwd);

        user.setCreateTime(new Date());
        userDao.insertSelective(user);
    }

    public User update(User user) {
        // 防止绕过页面提交表单修改不允许修改的数据
        user.setPassword(null);
        user.setSalt(null);
        user.setCreateTime(null);
        userDao.updateByPrimaryKeySelective(user);
        if (user.getAvatar() == null || user.getAvatar().equals("")) {
            userDao.updateAvatarByName(null, user.getName());
        }
        user.setToken(hostHolder.getUser().getToken());
        return user;
    }

    public void delete(Integer id) {
        userDao.deleteByPrimaryKey(id);
    }

    // 后台登录
    public User login(User admin, Integer userType, Params params) {
        // 1.进行非空判断
        if (admin.getName() == null || "".equals(admin.getName())) {
            throw new CustomException("用户名不能为空！");
        }
        if (admin.getPassword() == null || "".equals(admin.getPassword())) {
            throw new CustomException("密码不能为空！");
        }
        if (StrUtil.isBlank(params.getKaptchaOwner())) {
            throw new CustomException("请重新输入验证码");
        }
        if (StrUtil.isBlank(params.getVerifyCode())) {
            throw new CustomException("验证码不能为空！");
        }

        // 2.验证码判断
        String kaptcha = null;
        String kaptchaOwnerRedisKey = RedisKeyUtil.getKaptchaKey(params.getKaptchaOwner());
        kaptcha = (String) redisTemplate.opsForValue().get(kaptchaOwnerRedisKey);

        if (StrUtil.isBlank(kaptcha) || StrUtil.isBlank(params.getVerifyCode())
                || !kaptcha.equalsIgnoreCase(params.getVerifyCode())) {
            throw new CustomException("验证码有误！");
        }


        // 3.查询数据库，进行登录验证
        User foundUser = userDao.findByName(admin.getName());
        if (foundUser == null) {
            throw new CustomException("用户名或密码错误！");
        }
        if (foundUser.getType() != 1) {
            throw new CustomException("用户名或密码错误！");
        }
        String pwd = BBSUtil.md5(admin.getPassword() + foundUser.getSalt());
        if (!StrUtil.equals(pwd, foundUser.getPassword())) {
            throw new CustomException("用户名或密码错误！");
        }
        // 生成该登录用户对应的token，然后跟用户信息一同返回到前端
        String token = JwtTokenUtils.genToken(foundUser.getId().toString(), foundUser.getPassword());
        foundUser.setToken(token);
        return foundUser;
    }

    // 普通登录
    public User login(User user, Params params) {
        // 1.进行非空判断
        if (user.getName() == null || "".equals(user.getName())) {
            throw new CustomException("用户名不能为空！");
        }
        if (user.getPassword() == null || "".equals(user.getPassword())) {
            throw new CustomException("密码不能为空！");
        }
        if (StrUtil.isBlank(params.getKaptchaOwner())) {
            throw new CustomException("请重新输入验证码");
        }
        if (StrUtil.isBlank(params.getVerifyCode())) {
            throw new CustomException("验证码不能为空！");
        }

        // 2.验证码判断
        String kaptcha = null;
        String kaptchaOwnerRedisKey = RedisKeyUtil.getKaptchaKey(params.getKaptchaOwner());
        kaptcha = (String) redisTemplate.opsForValue().get(kaptchaOwnerRedisKey);

        if (StrUtil.isBlank(kaptcha) || StrUtil.isBlank(params.getVerifyCode())
                || !kaptcha.equalsIgnoreCase(params.getVerifyCode())) {
            throw new CustomException("验证码有误！");
        }

        // 3.查询数据库，进行登录验证
        User foundUser = userDao.findByName(user.getName());
        if (foundUser == null) {
            throw new CustomException("用户名或密码错误！");
        }
        String pwd = BBSUtil.md5(user.getPassword() + foundUser.getSalt());
        if (!StrUtil.equals(pwd, foundUser.getPassword())) {
            throw new CustomException("用户名或密码错误！");
        }
        // 生成该登录用户对应的token，然后跟用户信息一同返回到前端
        String token = JwtTokenUtils.genToken(foundUser.getId().toString(), foundUser.getPassword());
        foundUser.setToken(token);
        return foundUser;
    }

    public User findById(Integer id) {
        User user = userDao.selectByPrimaryKey(id);
        return user != null ? user : new User();
    }

    public Integer updateUserPwd(Password password) {
        User user = hostHolder.getUser();
        if (!Objects.equals(user.getId(), Integer.valueOf(password.getUserId()))) {
            throw new CustomException("非法操作");
        }
        if (StrUtil.isBlank(password.getNewPwd())) {
            throw new CustomException("新密码不能为空");
        }
        if (Objects.equals(password.getPresentPwd(), password.getNewPwd())) {
            throw new CustomException("新密码不能与原密码相同");
        }
        if (!Objects.equals(password.getNewPwd(), password.getAckNewPwd())) {
            throw new CustomException("两次输入的新密码不一致");
        }
        String pwd = user.getPassword();
        if (!Objects.equals(BBSUtil.md5(password.getPresentPwd() + user.getSalt()), pwd)) {
            throw new CustomException("原密码输入错误");
        }
        password.setNewPwd(BBSUtil.md5(password.getNewPwd() + user.getSalt()));
        return userDao.updateUserPwd(password);
    }

    public PageInfo<User> findByIdS(Params params, List<String> userIds) {
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        if (userIds == null || userIds.isEmpty()) {
            return PageInfo.of(new ArrayList<>());
        }
        List<User> list = userDao.findByIds(params, userIds);
        for (User u : list) {
            BBSUtil.userMasking(u);
            FollowParams followParams = new FollowParams();
            followParams.setFollowCount(followService.findFollowCountByUser(String.valueOf(u.getId())));
            followParams.setFollowStatus(followService.findFollowStatus(String.valueOf(u.getId())));
            u.setFollowParams(followParams);
        }
        return PageInfo.of(list);
    }

    /*public int register(User admin) {
        int result = 0;
        try {
            result = adminDao.insert(admin);
        } catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }*/
}
