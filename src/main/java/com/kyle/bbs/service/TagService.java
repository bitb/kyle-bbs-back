package com.kyle.bbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kyle.bbs.dao.TagDao;
import com.kyle.bbs.entity.Tag;
import com.kyle.bbs.entity.transientEntities.Params;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TagService {

    @Resource
    private TagDao tagDao;

    public List<Tag> findTagsByPostId(Integer postId, Integer status) {
        return tagDao.findTagsByPostId(postId, status);
    }

    public List<Tag> findALL(Params params) {
        List<Tag> list = tagDao.findAllTags(params);
        return list;
    }

    public PageInfo<Tag> findAllTags(Params params) {
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<Tag> list = tagDao.findAllTags(params);
        return PageInfo.of(list);
    }

    public void updatePostTags(List<Tag> tags, Integer postId) {
        tagDao.setTagsDeletedByPostId(postId);
        if (!tags.isEmpty()) {
            tagDao.insertAndUpdatePostTags(tags, postId);
        }
    }

    public void addPostTags(List<Tag> tags, Integer postId) {
        tagDao.insertPostTags(tags, postId);
    }

    public void addTag(Tag tag){
        tagDao.insertSelective(tag);
    }

    public void deleteTag(Integer id){
        tagDao.deleteByPrimaryKey(id);
    }

    public void updateTag(Tag tag) {
        tagDao.updateByPrimaryKeySelective(tag);
    }

}
