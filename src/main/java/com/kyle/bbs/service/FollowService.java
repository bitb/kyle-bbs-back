package com.kyle.bbs.service;

import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.dao.FollowDao;
import com.kyle.bbs.entity.Follow;
import com.kyle.bbs.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class FollowService implements BBSConstants {

    @Resource
    private FollowDao followDao;

    @Resource
    private HostHolder hostHolder;

    public Integer follow(String targetId) {
        User user = hostHolder.getUser();
        Follow follow = followDao.selectFollowByUserAndTarget(String.valueOf(user.getId()), targetId);
        if (follow != null) {
            if (Objects.equals(follow.getStatus(), FOLLOW_STATUS_FOLLOWED)) {
                followDao.updateFollowStatus(String.valueOf(user.getId()), targetId, FOLLOW_STATUS_UNFOLLOWED);
                return FOLLOW_STATUS_UNFOLLOWED;
            } else {
                followDao.updateFollowStatus(String.valueOf(user.getId()), targetId, FOLLOW_STATUS_FOLLOWED);
                return FOLLOW_STATUS_FOLLOWED;
            }
        } else {
            followDao.insertAndUpdateFollowStatus(String.valueOf(user.getId()), targetId);
            return FOLLOW_STATUS_UNFOLLOWED;
        }
    }

    public Integer findFollowStatus(String targetId) {
        User user = hostHolder.getUser();
        if (user == null) {
                return FOLLOW_STATUS_UNFOLLOWED;
        }
        Follow follow = followDao.selectFollowByUserAndTarget(String.valueOf(user.getId()), targetId);
        if (follow != null) {
            return Objects.equals(follow.getStatus(), FOLLOW_STATUS_FOLLOWED) ? FOLLOW_STATUS_FOLLOWED : FOLLOW_STATUS_UNFOLLOWED;
        } else {
            return FOLLOW_STATUS_UNFOLLOWED;
        }
    }

    public Long findFollowCountByUser(String targetId) {
        return followDao.selectUserFollowCount(targetId, FOLLOW_STATUS_FOLLOWED);
    }

    public List<String> findUserFollowedIds(String userId, Integer status) {
        return followDao.selectUserFollowedIds(userId, status);
    }
}
