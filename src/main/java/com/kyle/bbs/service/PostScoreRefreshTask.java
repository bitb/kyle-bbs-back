package com.kyle.bbs.service;

import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.entity.DiscussPost;
import com.kyle.bbs.utils.RedisKeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@Slf4j
public class PostScoreRefreshTask implements BBSConstants {

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private DiscussPostService discussPostService;

    @Resource
    private LikeService likeService;

    public static final Date epoch;
    static {
        try {
            epoch = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2024-01-01 00:00:00");
        } catch (ParseException e) {
            throw new RuntimeException("初始化Kyle纪年失败:" , e);
        }
    }

    @Scheduled(fixedRate = 1000 * 60 * 5)
    public void execute() {
        String redisKey = RedisKeyUtil.getPostScoreKey();
        BoundSetOperations operations = redisTemplate.boundSetOps(redisKey);

        if (operations.size() == 0) {
            log.info("任务取消，没有需要刷新的帖子。");
            return;
        }

        log.info("[任务开始，正在刷新帖子分数]" + operations.size());

        while (operations.size() > 0) {
            this.refresh((Integer) operations.pop());
        }

        log.info("[任务结束，帖子分数刷新完毕]" + operations.size());
    }

    private void refresh(int postId) {
        DiscussPost post = discussPostService.findSimplyById(postId);

        if (post == null) {
            log.error("该帖子不存在！id:" + postId);
            return;
        }

        // 评论数量
        int commentCount = post.getCommentCount();
        // 点赞数量
        long likeCount = likeService.findEntityLikeCount(ENTITY_TYPE_POST, postId);

        // 计算权重
        double w = commentCount * 10 + likeCount * 2;
        // 分数 = 帖子权重 + 距离天数
        double score = Math.log10(Math.max(w, 1)) + (post.getCreateTime().getTime() - epoch.getTime()) / (1000 * 3600 *24);
        // 更新帖子分数
        discussPostService.updateScore(postId, score);
    }
}
