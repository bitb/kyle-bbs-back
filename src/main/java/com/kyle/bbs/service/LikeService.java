package com.kyle.bbs.service;

import com.kyle.bbs.utils.RedisKeyUtil;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LikeService {

    @Resource
    private RedisTemplate redisTemplate;

    // 点赞
    public void like(Integer userId, Integer entityType, Integer entityId, Integer entityUserId){
        redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations operations) throws DataAccessException {
                String entityLikeKey = RedisKeyUtil.getEntityLikeKey(entityType, entityId);
                String userLikeKey = RedisKeyUtil.getUserLikeKey(entityUserId);
                String userLikedEntityKey = RedisKeyUtil.getUserLikedEntityKey(userId, entityType);

                boolean isMember = operations.opsForSet().isMember(entityLikeKey, userId);

                operations.multi();

                if(isMember){
                    operations.opsForSet().remove(entityLikeKey, userId);
                    operations.opsForList().remove(userLikedEntityKey, 0, entityId);
                    operations.opsForValue().decrement(userLikeKey);
                } else {
                    operations.opsForSet().add(entityLikeKey, userId);
                    operations.opsForList().rightPush(userLikedEntityKey, entityId);
                    operations.opsForValue().increment(userLikeKey);
                }

                return operations.exec();
            }
        });
    }

    // 查询实体点赞数
    public Long findEntityLikeCount(Integer entityType, Integer entityId){
        String entityLikeKey = RedisKeyUtil.getEntityLikeKey(entityType, entityId);
        return redisTemplate.opsForSet().size(entityLikeKey);
    }

    // 查询某用户点赞过的某类实体列表
    public List<String> findUserLikedPostIds(Integer userId, Integer entityType) {
        String userLikedKey = RedisKeyUtil.getUserLikedEntityKey(userId, entityType);
        return (List<String>) redisTemplate.opsForList().range(userLikedKey, 0, -1);
    }

    // 查询某用户对某实体的点赞状态
    public Boolean findEntityLikeStatus(Integer userId, Integer entityType, Integer entityId){
        String entityLikeKey = RedisKeyUtil.getEntityLikeKey(entityType, entityId);
        return redisTemplate.opsForSet().isMember(entityLikeKey, userId) ? true : false;
    }

    // 查询某用户获得的赞
    public Integer findUserLikeCount(Integer userId){
        String userLikeKey = RedisKeyUtil.getUserLikeKey(userId);
        Integer count = (Integer) redisTemplate.opsForValue().get(userLikeKey);
        return count == null ? 0 : count.intValue();
    }

}
