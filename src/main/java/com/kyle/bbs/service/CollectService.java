package com.kyle.bbs.service;

import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.dao.CollectDao;
import com.kyle.bbs.dao.FollowDao;
import com.kyle.bbs.entity.Collect;
import com.kyle.bbs.entity.Follow;
import com.kyle.bbs.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class CollectService implements BBSConstants {

    @Resource
    private CollectDao collectDao;

    @Resource
    private HostHolder hostHolder;

    public Integer collect(Integer entityType, String entityId) {
        User user = hostHolder.getUser();
        Collect collection = collectDao.selectCollectByUserAndTarget(String.valueOf(user.getId()), entityType, entityId);
        if (collection != null) {
            if (Objects.equals(collection.getStatus(), COLLECTION_STATUS_COLLECTED)) {
                collectDao.updateCollectStatus(String.valueOf(user.getId()), entityType, entityId, COLLECTION_STATUS_UNCOLLECTED);
                return COLLECTION_STATUS_UNCOLLECTED;
            } else {
                collectDao.updateCollectStatus(String.valueOf(user.getId()), entityType, entityId, COLLECTION_STATUS_COLLECTED);
                return COLLECTION_STATUS_COLLECTED;
            }
        } else {
            collectDao.insertAndUpdateCollectStatus(String.valueOf(user.getId()), entityType, entityId);
            return COLLECTION_STATUS_UNCOLLECTED;
        }
    }

    public Integer findCollectStatus(Integer entityType, String entityId) {
        User user = hostHolder.getUser();
        if (user == null) {
                return FOLLOW_STATUS_UNFOLLOWED;
        }
        Collect collection = collectDao.selectCollectByUserAndTarget(String.valueOf(user.getId()), entityType, entityId);
        if (collection != null) {
            return Objects.equals(collection.getStatus(), COLLECTION_STATUS_COLLECTED) ?
                    FOLLOW_STATUS_FOLLOWED : FOLLOW_STATUS_UNFOLLOWED;
        } else {
            return FOLLOW_STATUS_UNFOLLOWED;
        }
    }

    public Long findCollectCountByEntity(Integer entityType, String entityId) {
        return collectDao.selectEntityCollectCount(entityType, entityId, COLLECTION_STATUS_COLLECTED);
    }

    public List<String> findUserCollectedEntityIds(String userId, Integer entityType, Integer status) {
        return collectDao.selectUserCollectedEntityIds(userId, entityType, status);
    }
}
