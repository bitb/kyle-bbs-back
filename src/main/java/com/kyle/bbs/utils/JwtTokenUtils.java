package com.kyle.bbs.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.exception.CustomException;
import com.kyle.bbs.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Objects;

@Component
public class JwtTokenUtils implements BBSConstants {

    private static UserService staticUserService;
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtils.class);

    @Resource
    private UserService userService;

    @PostConstruct
    public void setUserService(){
        staticUserService = userService;
    }

    /**
     * 生成token
     */
    public static String genToken(String userId, String sign) {
        return JWT.create().withAudience(userId) // 将user id保存到token里面，作为载荷
                .withExpiresAt(DateUtil.offsetHour(new Date(), 2)) // 设置2小时为token过期时间
                .sign(Algorithm.HMAC256(sign)); // 以password(sign)作为token的密钥(签名)
    }

    public boolean verifyToken(String token, User user) {
        try {
            JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(user.getPassword())).build();
            jwtVerifier.verify(token); // 验证token
        } catch (JWTVerificationException e) {
            //throw new CustomException("token验证失败，请重新登录", Objects.equals(user.getType(), USER_TYPE_ADMIN) ? NEED_LOGIN_ADMIN : NEED_LOGIN_COMMON);
            return false;
        }
        return true;
    }

    /**
     * 获取当前登录的用户信息
     */
    public static User getCurrentUser() {
        String token = null;
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            token = request.getHeader("token");
            if (StrUtil.isBlank(token)) {
                token = request.getParameter("token");
            }
            if (StrUtil.isBlank(token)) {
                logger.error("获取当前登录的token失败，token:{}", token);
                return null;
            }
            // 解析token，获取用户ID
            String adminId = JWT.decode(token).getAudience().get(0);
            return staticUserService.findById(Integer.valueOf(adminId));
        }catch (Exception e) {
            logger.error("获取当前登录的管理员信息失败，token:{}", token, e);
            return null;
        }
    }

}
