package com.kyle.bbs.utils;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.kyle.bbs.entity.User;

public class BBSUtil {

    // 生成随机UUID
    public static String generateUUID() {
        return IdUtil.simpleUUID();
    }

    // MD5加密
    public static String md5(String key) {
        if (StrUtil.isBlank(key)) {
            return null;
        }
        return DigestUtil.md5Hex(key);
    }

    // 用户敏感信息隐藏
    public static User userMasking(User user) {
        user.setSalt(null);
        user.setPassword(null);

        return user;
    }
}
