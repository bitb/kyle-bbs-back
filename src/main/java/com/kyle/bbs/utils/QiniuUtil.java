package com.kyle.bbs.utils;

import cn.hutool.json.JSONUtil;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.exception.CustomException;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

public class QiniuUtil implements BBSConstants {

    private static final Logger logger = LoggerFactory.getLogger(QiniuUtil.class);

    //    上传文件
    public static String uploadQiNiu(MultipartFile file, String fileName, String bucketName, String qiniuUrl, User user) {

        String time = System.currentTimeMillis() + "";
        String flag = time + "--" + fileName;

        //  构造一个带指定Zone对象的配置了，  Zone.zone0() 代表华东区   Zone.zone1() 代表华北区  Zone.zone2() 代表华南区
        Configuration cfg = new Configuration(Region.huanan());
        UploadManager uploadManager = new UploadManager(cfg);

        try {
            //  默认不指定key的情况下，以文件内容的hash值作为文件名
            Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
            String token = auth.uploadToken(bucketName);
            Response response = uploadManager.put(file.getInputStream(), flag, token, null, null);
            // 解析上传成功的结果
            String bodyString = response.bodyString();
            Map<String, Object> map = JSONUtil.parseObj(bodyString);
            /*if (map.get("code") == null || !map.get("code").toString().equals("0")) {
                return null;
            }*/
            String hash = (String) map.get("key");
            logger.info("用户{" + user.getName() + "}上传文件成功： [http://" + qiniuUrl + "/" + flag + "]");

            return "http://" + qiniuUrl + "/" + flag;

        } catch (QiniuException e) {
            Response r = e.response;
            throw new CustomException("图片上传失败" + r.toString());
        } catch (IOException e) {
            logger.error("获取文件流失败:" + flag + " " + e);
        }
        return null;
    }
}
