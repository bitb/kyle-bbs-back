package com.kyle.bbs.controller;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.entity.Comment;
import com.kyle.bbs.entity.DiscussPost;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.entity.transientEntities.CollectParams;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.entity.Picture;
import com.kyle.bbs.exception.CustomException;
import com.kyle.bbs.service.*;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@CrossOrigin
@RestController
@RequestMapping("/discusspost")
public class DiscussPostController implements BBSConstants {

    @Resource
    private DiscussPostService discussPostService;

    @Resource
    private PictureService pictureService;

    @Resource
    private CommentService commentService;

    @Resource
    private TagService tagService;

    @Resource
    private LikeService likeService;

    @Resource
    private HostHolder hostHolder;

    @Resource
    private CollectService collectService;

    @GetMapping("/search")
    public Result findBySearch(Params params) {
        PageInfo<DiscussPost> info;
        params.setStatus(POST_STATUS_OK);
        if (params.getTagId() == null || params.getTagId() == 0) {
            info = discussPostService.findBySearch(params);
        } else {
            info = discussPostService.findByTag(params);
        }
        return Result.success(info);
    }

    @RequiresRoles("admin")
    @GetMapping("/admin/search")
    public Result findBySearchForAdmin(Params params) {
        PageInfo<DiscussPost> info;
        if (params.getTagId() != null) {
            if (params.getTagId() != 0) {
                info = discussPostService.findByTag(params);
            } else {
                info = discussPostService.findBySearch(params);
            }
        } else {
            info = discussPostService.findBySearch(params);
        }
        return Result.success(info);
    }

    @GetMapping("/userPost")
    public Result findByUserId(Params params) {
        params.setStatus(POST_STATUS_OK);
        PageInfo<DiscussPost> info = discussPostService.findPostByUserId(params);
        return Result.success(info);
    }

    @GetMapping("/likedPost")
    @RequiresAuthentication
    public Result findLikePostList(Params params) {
        User user = hostHolder.getUser();
        List<String> postIds = likeService.findUserLikedPostIds(user.getId(), ENTITY_TYPE_POST);
        PageInfo<DiscussPost> info = discussPostService.findPostByIds(params, postIds);
        return Result.success(info);
    }

    @GetMapping("/collectedPost")
    @RequiresAuthentication
    public Result findCollectedPostList(Params params) {
        User user = hostHolder.getUser();
        List<String> postIds = collectService.
                findUserCollectedEntityIds(String.valueOf(user.getId()), ENTITY_TYPE_POST, COLLECTION_STATUS_COLLECTED);
        PageInfo<DiscussPost> info = discussPostService.findPostByIds(params, postIds);
        return Result.success(info);
    }

    @GetMapping("/commentedPost")
    @RequiresAuthentication
    public Result findCommentedPostList(Params params) {
        User user = hostHolder.getUser();
        List<String> postIds = commentService.
                findUserCommentedPostIds(String.valueOf(user.getId()), COMMENT_STATUS_OK);
        PageInfo<DiscussPost> info = discussPostService.findPostByIds(params, postIds);
        return Result.success(info);
    }

    @GetMapping("/detail/{discussPostId}")
    public Result findById(@PathVariable Integer discussPostId, Params params) {
        // 查询帖子内容
        DiscussPost discussPost = discussPostService.findById(discussPostId);

        if (discussPost == null) {
            return Result.success();
        }

        // 查询评论列表
        PageInfo<Comment> commentPageInfo = commentService
                .findCommentByEntity(COMMENT_TYPE_COMMENT, String.valueOf(discussPost.getId()), params);
        discussPost.setComments(commentPageInfo);
        discussPost.setTags(tagService.findTagsByPostId(discussPostId, POST_TAG_STATUS_OK));

        return Result.success(discussPost);
    }

    @GetMapping("/top")
    public Result findTopPostList(Params params) {
        List<DiscussPost> list = discussPostService.findTopPostList();
        return Result.success(list);
    }

    @RequiresAuthentication
    @PostMapping
    public Result save(@RequestBody DiscussPost discussPost) {
        Integer postId;
        if (StrUtil.isBlank(discussPost.getTitle())) {
            throw new CustomException("标题不能为空");
        }
        if (StrUtil.isBlank(discussPost.getContent())) {
            throw new CustomException("内容不能为空");
        }
        if (discussPost.getId() == null) {
            discussPostService.add(discussPost);
            if (discussPost.getPictures() != null) {
                pictureService.add(discussPost.getPictures(), ENTITY_TYPE_POST, String.valueOf(discussPost.getId()));
            }
            if (discussPost.getTags() != null) {
                tagService.addPostTags(discussPost.getTags(), discussPost.getId());
            }
        } else {
            discussPostService.update(discussPost);
            if (discussPost.getPictures() == null) {
                List<Picture> emptyPictureList = new ArrayList<>();
                discussPost.setPictures(emptyPictureList);
            }
            tagService.updatePostTags(discussPost.getTags(), discussPost.getId());
            pictureService.update(discussPost.getPictures(), ENTITY_TYPE_POST, String.valueOf(discussPost.getId()));
        }
        postId = discussPost.getId();
        return Result.success(postId);
    }

    @RequiresRoles(value = { "admin" })
    @PostMapping("/adminSave")
    public Result adminSave(@RequestBody DiscussPost discussPost) {
        Integer postId;
        if (StrUtil.isBlank(discussPost.getTitle())) {
            throw new CustomException("标题不能为空");
        }
        if (StrUtil.isBlank(discussPost.getContent())) {
            throw new CustomException("内容不能为空");
        }
        if (discussPost.getId() == null) {
            discussPostService.add(discussPost);
            if (discussPost.getPictures() != null) {
                pictureService.add(discussPost.getPictures(), ENTITY_TYPE_POST, String.valueOf(discussPost.getId()));
            }
            if (discussPost.getTags() != null) {
                tagService.addPostTags(discussPost.getTags(), discussPost.getId());
            }
        } else {
            discussPostService.adminUpdate(discussPost);
            if (discussPost.getPictures() == null) {
                List<Picture> emptyPictureList = new ArrayList<>();
                discussPost.setPictures(emptyPictureList);
            }
            tagService.updatePostTags(discussPost.getTags(), discussPost.getId());
            pictureService.update(discussPost.getPictures(), ENTITY_TYPE_POST, String.valueOf(discussPost.getId()));
        }
        postId = discussPost.getId();
        return Result.success(postId);
    }

    @RequiresRoles(value = {"admin", "moderator"}, logical = Logical.OR)
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        discussPostService.updateStatus(id, POST_STATUS_DELETED);
        /*List<Picture> emptyPictureList = new ArrayList<>();
        pictureService.update(emptyPictureList, ENTITY_TYPE_POST, String.valueOf(id));*/
        return Result.success();
    }

    @RequiresAuthentication
    @DeleteMapping("/userDel/{id}")
    public Result userDel(@PathVariable Integer id) {
        User user = hostHolder.getUser();
        DiscussPost discussPost = discussPostService.findById(id);
        if (discussPost == null || Objects.equals(discussPost.getStatus(), POST_STATUS_DELETED)) {
            return Result.error("删除的帖子不存在！");
        }
        if (!Objects.equals(user.getId(), Integer.valueOf(discussPost.getUserId()))) {
            return Result.error("非法操作！");
        }else {
            discussPostService.updateStatus(id, POST_STATUS_DELETED);
            return Result.success();
        }
    }

    @RequiresRoles("admin")
    @PutMapping("/delBatch")
    public Result delBatch(@RequestBody List<DiscussPost> postList) {
        Integer ops = 0;
        for (DiscussPost discussPost : postList) {
            discussPostService.updateStatus(discussPost.getId(), BBSConstants.POST_STATUS_DELETED);
            /*List<Picture> emptyPictureList = new ArrayList<>();
            pictureService.update(emptyPictureList, ENTITY_TYPE_POST, String.valueOf(discussPost.getId()));*/
            ops++;
        }
        return Result.success(ops);
    }

    @RequiresAuthentication
    @PostMapping("/collect")
    public Result collect(@RequestBody CollectParams collectParams) {
        CollectParams newParams = new CollectParams();
        newParams.setCollectStatus(collectService.collect(ENTITY_TYPE_POST, collectParams.getEntityId()));
        newParams.setCollectCount(collectService.findCollectCountByEntity(ENTITY_TYPE_POST, collectParams.getEntityId()));
        return Result.success(newParams);
    }
}
