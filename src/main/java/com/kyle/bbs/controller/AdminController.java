package com.kyle.bbs.controller;

import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.service.UserService;
import com.kyle.bbs.utils.BBSUtil;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController implements BBSConstants {

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Resource
    private UserService userService;

    @PostMapping("/login")
    public Result login(@RequestBody User user, Params params){
        User loginUser = userService.login(user, USER_TYPE_ADMIN, params);
        BBSUtil.userMasking(loginUser);
        return Result.success(loginUser);
    }

    /**
     * 不允许注册一个管理员用户
     * @param user
     * @return
     */
    @Deprecated
    @RequiresRoles("admin")
    @PostMapping("/register")
    public Result register(@RequestBody User user){
        //userService.add(user);
        return Result.success();
    }

    @RequiresRoles("admin")
    @PostMapping("/update/user")
    public Result save(@RequestBody User user) {
        if (user.getId() == null) {
            user.setType(USER_TYPE_ADMIN);
            userService.add(user);
        } else {
            userService.update(user);
        }
        return Result.success();
    }

    @RequiresRoles("admin")
    @GetMapping
    public Result findAll(){
        List<User> userList = userService.findAll();
        return Result.success(userList);
    }

    @RequiresRoles("admin")
    @GetMapping("/search")
    public Result findBySearch(Params params) {
        params.setUserType(USER_TYPE_ADMIN);
        PageInfo<User> info = userService.findBySearch(params);
        return Result.success(info);
    }

    @RequiresRoles("admin")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        userService.delete(id);
        return Result.success();
    }


    /*@GetMapping("/register")
    public String register(){
        User admin = new User();
        admin.setName("123");
        int result = adminService.register(admin);
        return String.valueOf(result);
    }*/
}
