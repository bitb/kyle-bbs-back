package com.kyle.bbs.controller;

import cn.hutool.json.JSONUtil;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.common.factory.EntityTypeFactory;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.entity.transientEntities.LikeParams;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.service.LikeService;
import com.kyle.bbs.utils.RedisKeyUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@CrossOrigin
@RequestMapping
public class LikeController implements BBSConstants {

    @Resource
    private LikeService likeService;

    @Resource
    private HostHolder hostHolder;

    @Resource
    private RedisTemplate redisTemplate;

    @RequiresAuthentication
    @PostMapping("/like/{entityType}")
    public Result like(@PathVariable Integer entityType, @RequestBody LikeParams params) {

        User user = hostHolder.getUser();
        Integer entityId = Integer.valueOf(params.getEntityId());
        Integer postId = Integer.valueOf(params.getPostId());

        // 点赞
        likeService.like(user.getId(), entityType, entityId, Integer.valueOf(params.getEntityOwnerId()));
        // 点赞数量
        Long likeCount = likeService.findEntityLikeCount(entityType, entityId);
        // 点赞状态
        Boolean likeStatus = likeService.findEntityLikeStatus(user.getId(), entityType, entityId);

        if (entityType.equals(ENTITY_TYPE_POST) || entityType.equals(ENTITY_TYPE_COMMENT)) {
            String likeScoreRedisKey = RedisKeyUtil.getPostScoreKey();
            redisTemplate.opsForSet().add(likeScoreRedisKey, params.getEntityId());
        }

        LikeParams likeParams = new LikeParams();
        likeParams.setLikeCount(likeCount);
        likeParams.setLikeStatus(likeStatus);

        return Result.success(likeParams);
    }

}
