package com.kyle.bbs.controller;

import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.entity.DiscussPost;
import com.kyle.bbs.entity.Password;
import com.kyle.bbs.entity.transientEntities.FollowParams;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.service.DiscussPostService;
import com.kyle.bbs.service.FollowService;
import com.kyle.bbs.service.LikeService;
import com.kyle.bbs.service.UserService;
import com.kyle.bbs.utils.BBSUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController implements BBSConstants {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserService userService;

    @Resource
    private LikeService likeService;

    @Resource
    private HostHolder hostHolder;

    @Resource
    private DiscussPostService discussPostService;

    @Resource
    private FollowService followService;

    @PostMapping("/register")
    public Result save(@RequestBody User user) {
        if (user.getId() == null) {
            userService.add(user);
        } else {
            return Result.error("非法操作！");
        }
        return Result.success();
    }

    @RequiresAuthentication
    @PostMapping("/updateInfo")
    public Result updateUserInfo(@RequestBody User user) {
        return Result.success(userService.update(user));
    }

    @RequiresAuthentication
    @GetMapping("/search")
    public Result findBySearch(Params params) {
        params.setUserType(null);
        PageInfo<User> info = userService.findBySearch(params);
        return Result.success(info);
    }

    @GetMapping("/userInfo/{id}")
    public Result findUserInfoById(@PathVariable String id) {
        if (Integer.valueOf(id) == -1){
            return Result.success(null);
        }
        User user = userService.findById(Integer.valueOf(id));
        BBSUtil.userMasking(user);

        Params postCountParams = new Params();
        postCountParams.setStatus(POST_STATUS_OK);
        user.setGetLikeCount(likeService.findUserLikeCount(Integer.valueOf(id)));

        user.setPostCount(discussPostService.findUserPostCount(Integer.valueOf(id), postCountParams));

        FollowParams followParams = new FollowParams();
        followParams.setFollowCount(followService.findFollowCountByUser(id));
        followParams.setFollowStatus(followService.findFollowStatus(id));
        user.setFollowParams(followParams);

        return Result.success(user);
    }

    @RequiresAuthentication
    @PostMapping("/pwd")
    public Result updateUserPwd(@RequestBody Password password) {
        Integer result = userService.updateUserPwd(password);
        return Result.success(result);
    }

    @RequiresAuthentication
    @PostMapping("/follow")
    public Result follow(@RequestBody FollowParams followParams) {
        FollowParams newParams = new FollowParams();
        newParams.setFollowStatus(followService.follow(followParams.getTargetId()));
        newParams.setFollowCount(followService.findFollowCountByUser(followParams.getTargetId()));
        return Result.success(newParams);
    }

    @GetMapping("/followedUsers")
    @RequiresAuthentication
    public Result findFollowedUserList(Params params) {
        User user = hostHolder.getUser();
        List<String> userIds = followService.findUserFollowedIds(String.valueOf(user.getId()), FOLLOW_STATUS_FOLLOWED);
        PageInfo<User> info = userService.findByIdS(params, userIds);
        return Result.success(info);
    }
}
