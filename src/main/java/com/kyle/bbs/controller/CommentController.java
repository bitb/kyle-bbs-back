package com.kyle.bbs.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.entity.Comment;
import com.kyle.bbs.entity.DiscussPost;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.exception.CustomException;
import com.kyle.bbs.service.CommentService;
import com.kyle.bbs.service.DiscussPostService;
import com.kyle.bbs.utils.RedisKeyUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@CrossOrigin
@RestController
@RequestMapping("/comment")
public class CommentController implements BBSConstants {

    @Resource
    private DiscussPostService discussPostService;

    @Resource
    private CommentService commentService;

    @Resource
    private HostHolder hostHolder;

    @Resource
    private RedisTemplate redisTemplate;

    @RequiresAuthentication
    @PostMapping("/add")
    public Result addComment(@RequestBody Comment comment) {

        if (StrUtil.isBlank(comment.getContent())) {
            throw new CustomException("不允许发送内容为空的评论");
        }

        User user = hostHolder.getUser();

        // 校验评论帖子合法性
        DiscussPost discussPost = discussPostService.findById(Integer.valueOf(comment.getPostId()));
        if (ObjectUtil.isEmpty(discussPost)) {
            throw new CustomException("要回复的帖子不存在");
        } else if (!POSTS_AVAILABLE_FOR_NOT_ADMINS.contains(discussPost.getStatus())) {
            throw new CustomException("无法对目标帖子进行评论和回复");
        }

        // 添加评论到数据库
        comment.setUserId(String.valueOf(user.getId()));
        comment.setStatus(COMMENT_STATUS_OK);
        comment.setCreateTime(new Date());
        commentService.addComment(comment);

        int commentCount = commentService.findCommentCount(comment.getPostId());
        discussPost.setCommentCount(commentCount);
        discussPostService.update(discussPost);

        String likeScoreRedisKey = RedisKeyUtil.getPostScoreKey();
        redisTemplate.opsForSet().add(likeScoreRedisKey, comment.getPostId());

        return Result.success();
    }

    @GetMapping("/{discussPostId}")
    public Result findComments(@PathVariable String discussPostId, Params params) {

        PageInfo<Comment> commentPageInfo = commentService.findCommentByEntity(ENTITY_TYPE_POST, discussPostId, params);

        return Result.success(commentPageInfo);
    }

    @GetMapping("/replies/{commentId}")
    public Result findReplies(@PathVariable String commentId, Params params) {

        PageInfo<Comment> replyPageInfo = commentService.findCommentByEntity(ENTITY_TYPE_COMMENT, commentId, params);

        return Result.success(replyPageInfo);
    }

    @RequiresRoles(value = {"admin", "moderator"}, logical = Logical.OR)
    @DeleteMapping("/delete/{entityId}")
    public Result deleteCommentOrReply(@PathVariable String entityId) {

        Integer result = commentService.updateStatus(entityId, COMMENT_STATUS_DELETED);

        DiscussPost discussPost = discussPostService.findById(Integer.valueOf(entityId));
        discussPost.setCommentCount(discussPost.getCommentCount() - result);
        discussPostService.update(discussPost);

        String likeScoreRedisKey = RedisKeyUtil.getPostScoreKey();
        redisTemplate.opsForSet().add(likeScoreRedisKey, entityId);

        return Result.success(result);
    }
}
