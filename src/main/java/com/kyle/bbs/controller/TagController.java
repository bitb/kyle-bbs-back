package com.kyle.bbs.controller;

import com.github.pagehelper.PageInfo;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.entity.Tag;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.service.TagService;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping
public class TagController implements BBSConstants {

    @Resource
    private TagService tagService;

    @RequiresRoles("admin")
    @GetMapping("/admin/tag/getAll")
    public Result getAllTagsForAdmin(Params params) {
        PageInfo<Tag> info = tagService.findAllTags(params);
        return Result.success(info);
    }

    @RequiresRoles("admin")
    @GetMapping("/admin/tag/ALL")
    public Result getAllTagsWithoutPagesAdmin(Params params) {
        params.setTagId(null);
        params.setName(null);
        List<Tag> list = tagService.findALL(params);
        return Result.success(list);
    }

    @GetMapping("/tag/ALL")
    public Result getAllTagsWithoutPagesCommon(Params params) {
        params.setTagId(null);
        params.setName(null);
        params.setStatus(TAG_STATUS_OK);
        List<Tag> list = tagService.findALL(params);
        return Result.success(list);
    }

    @GetMapping("/tag/getAll")
    public Result getAllTags(Params params) {
        params.setStatus(TAG_STATUS_OK);
        PageInfo<Tag> info = tagService.findAllTags(params);
        return Result.success(info);
    }

    @RequiresRoles("admin")
    @PostMapping("/admin/tag")
    public Result saveTag(@RequestBody Tag tag) {
        if (tag.getId() == null) {
            tagService.addTag(tag);
        } else {
            tagService.updateTag(tag);
        }
        return Result.success();
    }

    @RequiresRoles("admin")
    @DeleteMapping("/admin/tag/delete/{id}")
    public Result deleteTag(@PathVariable Integer id) {
        tagService.deleteTag(id);
        return Result.success();
    }
}
