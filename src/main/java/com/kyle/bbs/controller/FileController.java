package com.kyle.bbs.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.common.factory.QiniuUrlBucketFactory;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.exception.CustomException;
import com.kyle.bbs.utils.QiniuUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

/**
 * 文件上传接口
 */
@CrossOrigin
@RestController
@RequestMapping("/files")
@RequiresAuthentication
public class FileController implements BBSConstants {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    // 文件上传存储路径
    private static final String filePath = System.getProperty("user.dir") + "/file/";

    @Resource
    private HostHolder hostHolder;

    @Resource
    private QiniuUrlBucketFactory qiniuUrlBucketFactory;

    /**
     * 文件上传
     */
    @PostMapping("/upload/{entityType}")
    public Result upload(MultipartFile file, @PathVariable String entityType) {
        synchronized (FileController.class) {
            User user = hostHolder.getUser();
            // 校验上传类型
            String[] qiniuParams = qiniuUrlBucketFactory.getUrlBucket(entityType);
            if (qiniuParams == null) {
                logger.error("用户{" + user.getName() + "}上传文件失败：非法实体类型{" + entityType + "}");
                throw new CustomException("非法实体类型");
            }
            // 获取当前时间戳
            String flag = null;
            // 获取上传文件原始文件名
            String fileName = file.getOriginalFilename();
            try {
                /*if(!FileUtil.isDirectory(filePath)) {
                    FileUtil.mkdir(filePath);
                }*/
                // 文件存储形式：时间戳-文件名
                // FileUtil.writeBytes(file.getBytes(), filePath + flag + "-" + fileName);
                flag = QiniuUtil.uploadQiNiu(file, fileName, DISCUSSPOST_BUCKET_NAME, QINIU_DISCUSSPOST_URL, user);
                Thread.sleep(1L);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return Result.error(e.toString());
            }
            return Result.success(flag);
        }
    }

    /**
     * 获取文件
     */
    @Deprecated
    @GetMapping("/getFile/{flag}")
    public void avatarPath(@PathVariable String flag, HttpServletResponse response) {
        if(!FileUtil.isDirectory(filePath)) {
            FileUtil.mkdir(filePath);
        }
        OutputStream os;
        List<String> fileNames = FileUtil.listFileNames(filePath);
        String avatar = fileNames.stream().filter(name -> name.contains(flag)).findAny().orElse("");
        try {
            if (StrUtil.isNotEmpty(avatar)) {
                response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(avatar, "UTF-8"));
                response.setContentType("application/octet-stream");
                byte[] bytes = FileUtil.readBytes(filePath + avatar);
                os = response.getOutputStream();
                os.write(bytes);
                os.flush();
                os.close();
            }
        } catch (Exception e) {
            System.out.println("文件下载失败");
        }
    }

}
