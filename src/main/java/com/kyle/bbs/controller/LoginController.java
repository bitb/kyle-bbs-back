package com.kyle.bbs.controller;

import com.google.code.kaptcha.Producer;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.Result;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.entity.transientEntities.Kaptcha;
import com.kyle.bbs.entity.transientEntities.Params;
import com.kyle.bbs.exception.CustomException;
import com.kyle.bbs.service.UserService;
import com.kyle.bbs.utils.BBSUtil;
import com.kyle.bbs.utils.RedisKeyUtil;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@RestController
@RequestMapping
public class LoginController implements BBSConstants {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserService userService;

    @Resource
    private Producer kaptchaProducer;

    @Resource
    private RedisTemplate redisTemplate;

    @PostMapping("/login")
    public Result login(@RequestBody User user, Params params){
        User loginUser = userService.login(user, params);
        BBSUtil.userMasking(loginUser);
        return Result.success(loginUser);
    }

    @PostMapping("/register")
    public Result register(@RequestBody User user){
        userService.add(user);
        return Result.success();
    }

    @GetMapping("/kaptcha")
    public Result getKaptcha() {

        // 生成验证码
        String text = kaptchaProducer.createText();
        BufferedImage image = kaptchaProducer.createImage(text);

        // 验证码归属
        String kaptchaOwner = BBSUtil.generateUUID();
        String kaptchaOwnerRedisKey = RedisKeyUtil.getKaptchaKey(kaptchaOwner);
        redisTemplate.opsForValue().set(kaptchaOwnerRedisKey, text, 300, TimeUnit.SECONDS);

        // 将验证码信息返回到前端
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(image,"png",outputStream);
        } catch (IOException e) {
            throw new CustomException("获取验证码失败");
        }
        BASE64Encoder encoder = new BASE64Encoder();
        String encode = encoder.encode(outputStream.toByteArray());
        encode = "data:image/png;base64,"+encode;

        Kaptcha kaptcha = new Kaptcha();
        kaptcha.setKaptchaOwner(kaptchaOwner);
        kaptcha.setKaptchaEncode(encode);

        return Result.success(kaptcha);
    }
}
