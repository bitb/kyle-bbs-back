package com.kyle.bbs.config.shiro;

import com.auth0.jwt.JWT;
import com.kyle.bbs.common.BBSConstants;
import com.kyle.bbs.common.HostHolder;
import com.kyle.bbs.entity.JwtToken;
import com.kyle.bbs.entity.User;
import com.kyle.bbs.exception.CustomException;
import com.kyle.bbs.service.UserService;
import com.kyle.bbs.utils.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Slf4j
public class JwtShiroRealm extends AuthorizingRealm implements BBSConstants {

    @Resource
    private JwtTokenUtils jwtTokenUtils;

    @Resource
    private UserService userService;

    @Resource
    private HostHolder hostHolder;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        log.info("进入doGetAuthenticationInfo");
        //通过Token.getPrincipal()获取令牌
        String token = (String) authenticationToken.getPrincipal();

        // 获取token中的userId
        String userId;
        User user;
        try {
            userId = JWT.decode(token).getAudience().get(0);
            // 根据token中的userId查询数据库
            user = userService.findById(Integer.parseInt(userId));
        } catch (Exception e) {
            String errMsg = "token验证失败，请重新登录";
            log.error(errMsg + ", token=" + token, e);
            throw new CustomException(errMsg);
        }
        if (user == null) {
            throw new CustomException("用户不存在，请重新登录");
        }
        if(!jwtTokenUtils.verifyToken(token, user)){
            throw new AuthenticationException("token验证失败，请重新登录");
        }
        user.setToken(token);
        hostHolder.setUser(user);

        //创建认证对象SimpleAuthenticationInfo
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(token, token, getName());
        return info;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        log.info("进入doGetAuthorizationInfo，执行用户授权");
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        User user = hostHolder.getUser();
        if (user.getType().equals(USER_TYPE_ADMIN)) {
            info.addRole("admin");
        } else if (user.getType().equals(USER_TYPE_MODERATOR)) {
            info.addRole("moderator");
        } else if (user.getType().equals(USER_TYPE_COMMON)) {
            info.addRole("common");
        }
        return info;
    }

}
