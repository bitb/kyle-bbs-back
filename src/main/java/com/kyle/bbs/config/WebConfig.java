package com.kyle.bbs.config;

import com.kyle.bbs.interceptor.JwtInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Resource
    private JwtInterceptor jwtInterceptor;

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        // 指定controller统一的接口前缀：相当于在url上拼了个 /api变成/api/xxx
        configurer.addPathPrefix("/api", clazz -> clazz.isAnnotationPresent(RestController.class));
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtInterceptor).addPathPatterns("/api/**")
                /*.excludePathPatterns(
                        "/api/admin/login",
                        "/api/admin/register",
                        "/api/files/getFile/**",
                        "/api/kaptcha",
                        "/api/login",
                        "/api/register",
                        "/api/discusspost/search",
                        "/api/discusspost/detail/**",
                        "/api/discusspost/top",
                        "/api/tag/**",
                        "/api/users/userInfo/**")*/;
    }
}
