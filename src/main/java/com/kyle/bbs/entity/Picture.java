package com.kyle.bbs.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "pictures")
public class Picture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "entity_type")
    private Integer entityType;

    @Column(name = "entity_id")
    private String entityId;

    @Column(name = "flag")
    private String flag;
}
