package com.kyle.bbs.entity;

import com.github.pagehelper.PageInfo;
import com.kyle.bbs.entity.transientEntities.Params;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "target_id")
    private String targetId;

    @Column(name = "entity_type")
    private Integer entityType;

    @Column(name = "entity_id")
    private String entityId;

    @Column(name = "post_id")
    private String postId;

    @Column(name = "content")
    private String content;

    @Column(name = "status")
    private Integer status;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "like_count")
    private Long likeCount;

    @Transient
    private PageInfo<Comment> replies;

    @Transient
    private String nickName;

    @Transient
    private String targetName;

    @Transient
    private Params params;

    @Transient
    private String avatar;

}
