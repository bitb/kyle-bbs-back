package com.kyle.bbs.entity.transientEntities;

import lombok.Data;

@Data
public class CollectParams {
    private String entityId;
    private Long collectCount;
    private Integer collectStatus;
}
