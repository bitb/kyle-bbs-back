package com.kyle.bbs.entity.transientEntities;

import lombok.Data;

@Data
public class Kaptcha {

    private String kaptchaOwner;

    private String kaptchaEncode;
}
