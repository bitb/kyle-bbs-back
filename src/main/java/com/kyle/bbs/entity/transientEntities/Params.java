package com.kyle.bbs.entity.transientEntities;

import lombok.Data;

@Data
public class Params {

    private Integer id;
    private Integer tagId;
    private Integer status;
    private Integer userType;

    private String title;
    private String content;

    private String searchText;

    private String name;
    private String nickName;
    private String phone;

    private String kaptchaOwner;
    private String verifyCode;

    private String entityId;
    private String postId;
    private String userId;

    private String order;
    private String orderBy;
    private Integer limit;
    private Integer pageNum;
    private Integer pageSize;
}
