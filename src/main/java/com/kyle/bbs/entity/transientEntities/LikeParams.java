package com.kyle.bbs.entity.transientEntities;

import lombok.Data;

@Data
public class LikeParams {
    private Integer entityType;
    private String entityId;
    private String postId;
    private Long likeCount;
    private Boolean likeStatus;
    private String entityOwnerId;
}
