package com.kyle.bbs.entity.transientEntities;

import lombok.Data;

@Data
public class FollowParams {
    private String targetId;
    private Long followCount;
    private Integer followStatus;
}
