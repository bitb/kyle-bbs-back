package com.kyle.bbs.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Table(name = "Collect")
@Data
public class Collect {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "entity_type")
    private Integer entityType;

    @Column(name = "entity_id")
    private String entityId;

    @Column(name = "status")
    private Integer status;

    @Column(name = "update_time")
    private Date updateTime;

}
