package com.kyle.bbs.entity;

import com.github.pagehelper.PageInfo;
import com.kyle.bbs.entity.transientEntities.CollectParams;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Table(name = "discuss_post")
public class DiscussPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private String userId;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;
    @Column(name = "type")
    private Integer type;
    @Column(name = "status")
    private Integer status;
    @Column(name = "comment_count")
    private Integer commentCount;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "score")
    private Double score;
    @Column(name = "like_count")
    private Long likeCount;
    @Transient
    private List<Picture> pictures;
    @Transient
    private PageInfo<Comment> comments;
    @Transient
    private String nickName;
    @Transient
    private List<Tag> tags;
    @Transient
    private Boolean likeStatus;
    @Transient
    private CollectParams collectParams;
}
