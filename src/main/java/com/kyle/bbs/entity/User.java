package com.kyle.bbs.entity;

import com.kyle.bbs.entity.transientEntities.FollowParams;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;
    @Column(name = "age")
    private String age;
    @Column(name = "sex")
    private String sex;
    @Column(name = "email")
    private String email;
    @Column(name = "phone")
    private String phone;
    @Column(name = "address")
    private String address;
    @Column(name = "type")
    private Integer type;
    @Column(name = "status")
    private Integer status;
    @Column(name = "avatar")
    private String avatar;
    @Column(name = "salt")
    private String salt;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "nick_name")
    private String nickName;
    @Transient
    private String token;
    @Transient
    private Integer getLikeCount;
    @Transient
    private Integer postCount;
    @Transient
    private FollowParams followParams;
}
