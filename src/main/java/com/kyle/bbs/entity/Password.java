package com.kyle.bbs.entity;

import lombok.Data;

@Data
public class Password {

    String userId;
    String presentPwd;
    String newPwd;
    String ackNewPwd;
}
