package com.kyle.bbs;

import com.kyle.bbs.utils.BBSUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class KylebbsApplicationTests {


    @Test
    void contextLoads() {
        System.out.println(BBSUtil.md5("123456123"));
    }


    @Test
    void testPwd() {
        String pwd = "qwe";
        String encodedPwd = "cb1110a4ab1071bfed7e24936f53e53c";
        String salt = "9ed22";
        System.out.println(BBSUtil.md5(pwd + salt));
    }
}
